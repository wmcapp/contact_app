package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import entities.Contact;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static String dbName = "ContactDB";
    private static int dbVersion = 1;

    private static String contactTable = "contact";

    private static String contactid = "id";
    private static String contactname = "name";
    private static String contactphone = "phone";
    private static String contactaddress = "address";
    private static String contactemail = "email";
    private static String contactdescription = "description";

    public DatabaseHelper(Context context){
        super(context, dbName,null, dbVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL("create table " + contactTable + "(" +
                contactid + " integer primary key autoincrement, " +
                contactname + " tex, " +
                contactphone + " text, " +
                contactaddress + " text, " +
                contactemail + " text, " +
                contactdescription + " text " +
                ")"
        );

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("drop table if exists " + contactTable);
        onCreate(sqLiteDatabase);
    }

    public boolean create(Contact contact) {
        boolean result = true;
        try {
            SQLiteDatabase sqLiteDatabase = getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(contactname, contact.getName());
            contentValues.put(contactphone, contact.getPhone());
            contentValues.put(contactaddress, contact.getAddress());
            contentValues.put(contactemail, contact.getEmail());
            contentValues.put(contactdescription, contact.getDescription());
            result = sqLiteDatabase.insert(contactTable, null, contentValues) > 0;
        } catch (Exception e) {
            result = false;
        }
        return result;
    }

    public List<Contact> findAll() {
        List<Contact> contacts = null;
        try {
            SQLiteDatabase sqLiteDatabase = getReadableDatabase();
            Cursor cursor = sqLiteDatabase.rawQuery("select * from " + contactTable, null);
            if (cursor.moveToFirst()) {
                contacts = new ArrayList<Contact>();
                do {
                    Contact contact = new Contact();
                    contact.setId(cursor.getInt(0));
                    contact.setName(cursor.getString(1));
                    contact.setPhone(cursor.getString(2));
                    contact.setAddress(cursor.getString(3));
                    contact.setEmail(cursor.getString(4));
                    contact.setDescription(cursor.getString(5));
                    contacts.add(contact);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            contacts = null;
        }
        return contacts;
    }

    public Contact find(int id) {
        Contact contact = null;
        try {
            SQLiteDatabase sqLiteDatabase = getReadableDatabase();
            Cursor cursor = sqLiteDatabase.rawQuery("select * from " + contactTable + " where " + contactid + " = ?", new String[] { String.valueOf(id) });
            if (cursor.moveToFirst()) {
                contact = new Contact();
                do {
                    contact.setId(cursor.getInt(0));
                    contact.setName(cursor.getString(1));
                    contact.setPhone(cursor.getString(2));
                    contact.setAddress(cursor.getString(3));
                    contact.setEmail(cursor.getString(4));
                    contact.setDescription(cursor.getString(5));
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            contact = null;
        }
        return contact;
    }

    public boolean update(Contact contact) {
        boolean result = true;
        try {
            SQLiteDatabase sqLiteDatabase = getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(contactname, contact.getName());
            contentValues.put(contactphone, contact.getPhone());
            contentValues.put(contactaddress, contact.getAddress());
            contentValues.put(contactemail, contact.getEmail());
            contentValues.put(contactdescription, contact.getDescription());
            result = sqLiteDatabase.update(contactTable, contentValues, contactid + " = ?", new String[]{ String.valueOf(contact.getId()) }) > 0;
        } catch (Exception e) {
            result = false;
        }
        return result;
    }

    public boolean delete(int id) {
        boolean result = true;
        try {
            SQLiteDatabase sqLiteDatabase = getWritableDatabase();
            result = sqLiteDatabase.delete(contactTable, contactid + " = ?", new String[]{ String.valueOf(id) }) > 0;
        } catch (Exception e) {
            result = false;
        }
        return result;
    }

    public List<Contact> search(String keyword) {
        List<Contact> contacts = null;
        try {
            SQLiteDatabase sqLiteDatabase = getReadableDatabase();
            Cursor cursor = sqLiteDatabase.rawQuery("select * from " + contactTable + " where " + contactname + " like ?", new String[] { "%" + keyword + "%" });
            if (cursor.moveToFirst()) {
                contacts = new ArrayList<Contact>();
                do {
                    Contact contact = new Contact();
                    contact.setId(cursor.getInt(0));
                    contact.setName(cursor.getString(1));
                    contact.setPhone(cursor.getString(2));
                    contact.setAddress(cursor.getString(3));
                    contact.setEmail(cursor.getString(4));
                    contact.setDescription(cursor.getString(5));
                    contacts.add(contact);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            contacts = null;
        }
        return contacts;
    }


}
