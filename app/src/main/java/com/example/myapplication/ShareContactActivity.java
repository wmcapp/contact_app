package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import database.DatabaseHelper;
import entities.Contact;

public class ShareContactActivity extends AppCompatActivity {

    private TextView viewTextShareInfo;

    private ImageView imageViewResult;

    private Button buttonCancel;

    private Contact contact;

    private String shareContactInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_contact);
        initView();
        loadData();
    }

    private void initView() {
        viewTextShareInfo = findViewById(R.id.viewTextShareInfo);
        imageViewResult = findViewById(R.id.imageViewResult);
        buttonCancel = findViewById(R.id.buttonCancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonCancel_onClick(view);
            }
        });
    }

    private void loadData() {
        Intent intent = getIntent();
        int id = intent.getIntExtra("id", 0);
        DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
        contact = databaseHelper.find(id);
        shareContactInfo= contact.getName() + "\n";
        shareContactInfo+=contact.getPhone() + "\n";
        shareContactInfo+=contact.getEmail();

        viewTextShareInfo.setText(shareContactInfo);
        generateCode();
    }

    private void generateCode(){
        try {
            MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
            BitMatrix bitMatrix = multiFormatWriter.encode(shareContactInfo, BarcodeFormat.QR_CODE, 400, 400);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            imageViewResult.setImageBitmap(bitmap);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void buttonCancel_onClick(View view) {
        Intent intent = new Intent(ShareContactActivity.this, MainActivity.class);
        startActivity(intent);
    }

}
