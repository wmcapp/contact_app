package com.example.myapplication;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.SearchManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.List;

import adapters.ContactListAdapter;
import database.DatabaseHelper;
import entities.Contact;

public class MainActivity extends AppCompatActivity {

    private ListView listViewContact;
    //public static final String EXTRA_MESSAGE="com.example.myapplication.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        loadData();
    }

    private void initView(){
        listViewContact = findViewById(R.id.listViewContact);
        registerForContextMenu(listViewContact);
    }

    private void loadData(){
        DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
        List<Contact> contacts = databaseHelper.findAll();
        if (contacts != null) {
            listViewContact.setAdapter(new ContactListAdapter(getApplicationContext(), contacts));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.contact_menu, menu);
        SearchView searchView = (SearchView) menu.findItem(R.id.searchView).getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchContact(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchContact(newText);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.addContactMenu) {
            Intent intent = new Intent(MainActivity.this, AddContactActivity.class);
            startActivity(intent);
        }
        else if(item.getItemId() == R.id.receiveContactMenu) {
            scan();

     //       Intent intent = new Intent(MainActivity.this, ReceiveContactActivity.class);
       //     startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.crud_menu, contextMenu);
        super.onCreateContextMenu(contextMenu, view, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(final MenuItem item) {
        if (item.getItemId() == R.id.editContactMenu ) {
            AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
            TextView textViewId = adapterContextMenuInfo.targetView.findViewById(R.id.textViewId);
            int id = Integer.parseInt(textViewId.getText().toString());
            Intent intent = new Intent(MainActivity.this, EditContactActivity.class);
            intent.putExtra("id", id);
            startActivity(intent);
        }

        else if (item.getItemId() == R.id.deleteContactMenu) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.confirm);
            builder.setMessage(R.string.are_you_sure);
            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    try {
                        AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                        TextView textViewId = adapterContextMenuInfo.targetView.findViewById(R.id.textViewId);
                        int id = Integer.parseInt(textViewId.getText().toString());
                        DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
                        if (databaseHelper.delete(id)) {
                            loadData();
                        } else {
                            Toast.makeText(getApplicationContext(), getText(R.string.delete_failed), Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            });
            builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            builder.show();
        }

        else if (item.getItemId() == R.id.shareContactMenu ) {
            AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
            TextView textViewId = adapterContextMenuInfo.targetView.findViewById(R.id.textViewId);
            int id = Integer.parseInt(textViewId.getText().toString());
            Intent intent = new Intent(MainActivity.this, ShareContactActivity.class);
            intent.putExtra("id", id);
            startActivity(intent);
        }

        return super.onContextItemSelected(item);
    }

    private void searchContact(String keyword) {
        DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
        List<Contact> contacts = databaseHelper.search(keyword);
        if (contacts != null) {
            listViewContact.setAdapter(new ContactListAdapter(getApplicationContext(), contacts));
        }
    }

    private void scan(){
        IntentIntegrator intentIntegrator = new IntentIntegrator(this);
        intentIntegrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
        intentIntegrator.setCameraId(0);
        intentIntegrator.initiateScan();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult intentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(intentResult != null) {
            //String result = intentResult.getContents();
            Intent intent = new Intent(MainActivity.this, ReceiveContactActivity.class);
            intent.putExtra("data", intentResult.getContents());
            startActivity(intent);
        }
    }



    /*public void sendMessage(View view)
    {
        Intent intent = new Intent(this, DisplayMessageActivity.class);
        EditText editText = findViewById(R.id.editText);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }*/

}
