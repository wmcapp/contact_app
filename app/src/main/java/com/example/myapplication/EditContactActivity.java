package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Pattern;

import java.util.List;

import database.DatabaseHelper;
import entities.Contact;

public class EditContactActivity extends AppCompatActivity implements Validator.ValidationListener {

    @NotEmpty
    private EditText editTextName;

    @NotEmpty
    @Pattern(regex =  "^\\(?([0-9]{3})\\)?[-.\\s]?([0-9]{3})[-.\\s]?([0-9]{4})$")
    private EditText editTextPhone;

    @NotEmpty
    @Email
    private EditText editTextEmail;

    private EditText editTextAddress;

    private EditText editTextDescription;

    private Button buttonSave, buttonCancel;

    private Validator validator;

    private Contact contact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_contact);
        initView();
        loadData();
        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    private void initView() {
        editTextName = findViewById(R.id.editTextName);
        editTextPhone = findViewById(R.id.editTextPhone);
        editTextEmail = findViewById(R.id.editTextEmail);
        editTextAddress = findViewById(R.id.editTextAddress);
        editTextDescription = findViewById(R.id.editTextDescription);
        buttonSave = findViewById(R.id.buttonSave);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonSave_onClick(view);
            }
        });
        buttonCancel = findViewById(R.id.buttonCancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonCancel_onClick(view);
            }
        });
    }

    private void loadData() {
        Intent intent = getIntent();
        int id = intent.getIntExtra("id", 0);
        DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
        contact = databaseHelper.find(id);
        editTextName.setText(contact.getName());
        editTextPhone.setText(contact.getPhone());
        editTextEmail.setText(contact.getEmail());
        editTextAddress.setText(contact.getAddress());
        editTextDescription.setText(contact.getDescription());
    }

    private void buttonSave_onClick(View view) {
        validator.validate();
    }

    private void buttonCancel_onClick(View view) {
        Intent intent = new Intent(EditContactActivity.this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onValidationSucceeded() {
        DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
        contact.setName(editTextName.getText().toString());
        contact.setPhone(editTextPhone.getText().toString());
        contact.setEmail(editTextEmail.getText().toString());
        contact.setAddress(editTextAddress.getText().toString());
        contact.setDescription(editTextDescription.getText().toString());
        if(databaseHelper.update(contact)) {
            Intent intent = new Intent(EditContactActivity.this, MainActivity.class);
            startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(), getText(R.string.update_failed), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            // Display error messages
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

}
